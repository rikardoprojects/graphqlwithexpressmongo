/* server.js
Import express library in our file, allocate a distinct port an create an express server to listen on than port.
*/

const express = require('express');

const PORT = process.env.PORT || 3000;
const app = express();

app.listen(PORT);
console.log('GraphQL API Server up and running at localhost listening at port:' + PORT);